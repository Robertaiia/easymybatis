package com.myapp.dao;

import com.myapp.entity.Address;

import net.oschina.durcframework.easymybatis.dao.CrudDao;

public interface AddressDao extends CrudDao<Address> {

}